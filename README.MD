**READ ME**

These scripts originally came from the book "Automate The Boring Stuff", chapter 17 by Al Sweigart. I only modified them a bit to experiment.

[Link](https://automatetheboringstuff.com/chapter17/)
