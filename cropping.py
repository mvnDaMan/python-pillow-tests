from PIL import Image

catImage = Image.open("zophie.png")

# crop at (200, 200) to (300, 400), creating an image that is (100, 200) size
cropped = catImage.crop((200, 200, 300, 400))
cropped.save("cropped.png")
