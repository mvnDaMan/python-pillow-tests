from PIL import Image

pasteImage = Image.open("cropped.png")
NEW_WIDTH = 500
NEW_HEIGHT = 600
newImage = Image.new("RGBA", (NEW_WIDTH, NEW_HEIGHT))

for i in range(0, NEW_WIDTH, 100):
	for j in range(0, NEW_HEIGHT, 200):
		newImage.paste(pasteImage, (i, j))

newImage.save("wall.png")
