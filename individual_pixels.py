from PIL import Image
from PIL import ImageColor

image = Image.new("RGBA", (100, 100))

print("pixel @ (0, 0): {0}".format(image.getpixel((0, 0))))
# pixel @ (0, 0): (0, 0, 0, 0)

for x in range(100):
	for y in range(50):
		image.putpixel((x, y), (210, 210, 210))

# y = 50
for y in range(50, 100):
	for x in range(100):
		image.putpixel((x, y), (255, 128, 128))

image.save("individual_pixel_editing.png")


# Another one, with black background, and purple lines/rows
image2 = Image.new("RGBA", (100, 100), "black")

for y in range(0, 100, 5):
	for x in range(100):
		image2.putpixel((x, y), ImageColor.getcolor("purple", "RGBA"))

image2.save("individual_pixel_editing2.png")
