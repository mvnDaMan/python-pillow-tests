from PIL import Image
catImage = Image.open("zophie.png")
print(catImage.size)

imgWidth, imgHeight = catImage.size
print("image height and size: ({0}, {1})".format(imgWidth, imgHeight))

print("filename:", catImage.filename)
print("format:", catImage.format)
print("format description:", catImage.format_description)

# save as
catImage.save("zoph.jpg")
