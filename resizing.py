from PIL import Image

cat = Image.open("zophie.png")
width, height = cat.size

quarterSize = cat.copy()
quarterSize = quarterSize.resize((int(width / 2), int(height / 2)))
quarterSize.save("quarter.png")
