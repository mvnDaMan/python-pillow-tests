from PIL import Image, ImageDraw, ImageFont
import os

image = Image.new("RGBA", (200, 200), "white")
draw = ImageDraw.Draw(image)
draw.text((20, 150), "Hello", fill = "purple")

# using fonts:
fontsFolder = "/usr/share/fonts/truetype"
ubuntuFont = ImageFont.truetype(os.path.join(fontsFolder, "ubuntu-font-family/Ubuntu-R.ttf"), 32)

draw.text((100, 150), "Howdy", fill = "gray", font = ubuntuFont)
image.save("text.png")
