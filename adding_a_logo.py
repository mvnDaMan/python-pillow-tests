import os
from PIL import Image

SQUARE_FIT_SIZE = 300	# square to resize images to
LOGO_FILENAME = "logo.png"
SAVE_FOLDER_NAME = "withLogo"

catImage = Image.open("zophie.png")
logoImage = Image.open(LOGO_FILENAME)
logoWidth, logoHeight = logoImage.size

os.makedirs(SAVE_FOLDER_NAME, exist_ok = True)

for filename in os.listdir("."):
	if not (filename.endswith(".png") or filename.endswith(".jpg")) or filename == LOGO_FILENAME:
		continue
		
	image = Image.open(filename)
	width, height = image.size

	# Check if image needs to be resized
	if width > SQUARE_FIT_SIZE and height > SQUARE_FIT_SIZE:
		# Calculate the new width and height to resize to
		if width > height:
			height = int((SQUARE_FIT_SIZE / width) * height)
			width = SQUARE_FIT_SIZE
		else:
			width = int((SQUARE_FIT_SIZE / height) * width)
			height = SQUARE_FIT_SIZE
		
		# resize the image
		print("Resizing %s..." % filename)
		image = image.resize((width, height))
		
		# add the logo
		print("Adding logo to %s..." % filename)
		image.paste(logoImage, (width - logoWidth, height - logoHeight), logoImage)	# final parameter "logoImage" is mask. Significan pixels (pixels with colour) are copied, while those that don't aren't, masking alpha pixels and copying the image.
		
		image.save(os.path.join(SAVE_FOLDER_NAME, filename))
