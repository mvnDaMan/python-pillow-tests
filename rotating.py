from PIL import Image

catImage = Image.open("zophie.png")

i = 0
while i <= 270:
	catImage.rotate(i, expand = True).save("zophie_rotated_{0}.png".format(i))
	i += 90
	
i = 2
while i < 10:
	catImage.rotate(i, expand = True).save("zophie_rotated_{0}.png".format(i))
	i += 2
