from PIL import Image

catImage = Image.open("zophie.png")

catImage.transpose(Image.FLIP_LEFT_RIGHT).save("horizontal_flip.png")
catImage.transpose(Image.FLIP_TOP_BOTTOM).save("vertical_flip.png")
