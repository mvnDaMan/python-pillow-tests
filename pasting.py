from PIL import Image

pasteImage = Image.open("cropped.png")
targetImage = Image.open("zophie.png").copy()

targetImage.paste(pasteImage, (100, 100))
targetImage.save("pasted.png")
