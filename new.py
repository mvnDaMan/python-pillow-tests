from PIL import Image

img = Image.new("RGBA", (100, 200), "blue") # or Image.getColor("blue")
img.save("blueImage.png")

# transparent image:
img = Image.new("RGBA", (100, 200))
img.save("transparentImage.png")
